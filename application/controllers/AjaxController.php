<?php

class AjaxController extends Zend_Controller_Action
{
 	
	
	public function init()
    {

        $this->_helper->AjaxContext()->addActionContext('stat', 'json')->initContext('json');
        $this->_helper->AjaxContext()->addActionContext('deletelending', 'json')->initContext('json');


    }

    public function deletelendingAction(){

        if ($this->getRequest()->isXmlHttpRequest())
        {

            $landing = new Model_LandingData($this->getRequest()->getParam('id'));
           // $landing->delete();

        }
    }

    public function setpaymentstatusAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isXmlHttpRequest())
        {

            $payment = new Model_Payments($this->getRequest()->getParam('id'));
            $payment->status = $this->getRequest()->getParam('status');
            $payment->save();

        }

        $response = $this->getResponse();
        $response->setBody('["ok"]')
            ->setHeader('content-type', 'text/javascript', true);
    }

    public function setuserroleAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isXmlHttpRequest())
        {
            $owner = new Model_Owner((int)$this->getRequest()->getParam('owner_id'));
            $owner->role = $this->getRequest()->getParam('role');
            $owner->save();
        }


        $response = $this->getResponse();
        $response->setBody('["ok"]')
            ->setHeader('content-type', 'text/javascript', true);


    }
    public function setuserstatusAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isXmlHttpRequest())
        {
            $owner = new Model_Owner((int)$this->getRequest()->getParam('owner_id'));
            $owner->activated = (int)$this->getRequest()->getParam('activated');
            $owner->save();
        }

        $response = $this->getResponse();
        $response->setBody('["ok"]')
            ->setHeader('content-type', 'text/javascript', true);

    }

    public function showlogAction(){

        $array_statues = array('New'=>'Новый', 'Processing'=>'В работе', 'Approved'=>'Утвержденный', 'Declined'=>'Отмененный');

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isXmlHttpRequest())
        {

            $log = new Model_LandingLog();
            $data = $log->getLogs($this->getRequest()->getParam('id'));
            $output = '';
            if(count($data) == 0)
                $output = '<tr><td colspan=4 style="text-align:center;">Нет данных</td></tr>';
            else
                foreach($data as $d)
                   $output .= '<tr><td>'.$d['owner_name'].'</td><td>'.$d['date'].'</td><td>'.$array_statues[$d['status']].'</td><td>'.$d['costs'].'</td></tr>';
        }

        $response = $this->getResponse();
        $response->setBody($output)
            ->setHeader('content-type', 'text/javascript', true);

    }

    public function setstatusAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if ($this->getRequest()->isXmlHttpRequest())
        {

            $landing = new Model_LandingData($this->getRequest()->getParam('id'));

            $old_status = $landing->status;
            $old_income = $landing->income;

            $landing->status = $this->getRequest()->getParam('status');


            if($this->getRequest()->getParam('status') == 'Approved'){
                if($landing->getLandings()->costs != 0)
                    $landing->income = $landing->getLandings()->costs * ($landing->getOwner()->owner_rate/100);
                else
                    $landing->income = $this->getRequest()->getParam('costs') * ($landing->getOwner()->owner_rate/100);
            }else
                $landing->income = 0;


            if($this->getRequest()->getParam('status') == 'Declined')
                $landing->details = $this->getRequest()->getParam('details');
            else
                $landing->details = '';


            $landing->lastupdate = new Zend_Db_Expr('NOW()');
            $landing->details = $this->getRequest()->getParam('details');
            $id = $landing->save();


            $llog = new Model_LandingLog();
            $llog->lead_data_id = $id;
            $llog->owner_id = Zend_Auth::getInstance()->getIdentity()->owner_id;
            $llog->costs = $this->getRequest()->getParam('costs')?$this->getRequest()->getParam('costs'):0;
            $llog->status = $this->getRequest()->getParam('status');
            $llog->date = new Zend_Db_Expr('NOW()');


            $llog->save();

            $status_filed = array('New'=>'leads_new','Processing'=>'leads_new','Approved'=>'leads_approved', 'Declined'=>'leeds_declined');
            $db = Zend_Registry::get('db');
            $stmt = $db->prepare("UPDATE
                                        statistics
                                  SET
                                        ".$status_filed[$old_status]."=".$status_filed[$old_status]."-1,
                                        ".$status_filed[$this->getRequest()->getParam('status')]."=".$status_filed[$this->getRequest()->getParam('status')]."+1,
                                        income = income - ".$old_income.",
                                        income = income + ".$landing->income."

                                  WHERE
                                        day = DATE_FORMAT('".$landing->date."', '%Y-%m-%d') AND
                                        owner_id = ".$landing->owner_id." AND
                                        landing_id = ".$landing->lead_id);
            $stmt->execute();
        }
    }

    public function resetAction()
	{

		$search_form = new Zend_Session_Namespace('search_form');
		unset($search_form->_data);
		$this->_helper->redirector('index');
	}
	/*
	public function indexAction()
	{


	}*/

    public function registervisitorAction(){


        $db = Zend_Registry::get('db');


        $data = array(
            'owner_id'      => 16,
            'lead_id' => 1,
            'ip'      => $_SERVER["REMOTE_ADDR"]
        );

        $current_table = date('dmY', mktime(0, 0, 0, date("m"), date("d"), date("Y")));

        $db->insert($current_table, $data);

    }

}
