<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {

       $this->_helper->AjaxContext()->addActionContext('registerlanding', 'json')->initContext('json');
       $this->_helper->AjaxContext()->addActionContext('registervisitor', 'json')->initContext('json');

       $this->view->tab = $this->_getParam('action');

    }


    public function statAction(){
        print_r(3453451); exit;
    }

    public function getlendingdataAction(){


        $request = $this->getRequest();

        print_r($request->isGet()); exit;

        print_r($this->_getParam('username')); exit;

    }


    public function registerlandingAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if($this->getRequest()->isGet()){

            if($this->getRequest()->getParam('lead_id') && $this->getRequest()->getParam('owner_id')){

                $data  = array(

                    'owner_id'      => $this->getRequest()->getParam('owner_id'),
                    'lead_id' => $this->getRequest()->getParam('lead_id'),
                    'ip'      => $_SERVER["REMOTE_ADDR"],
                    'useragent' => $_SERVER['HTTP_USER_AGENT'],
                    'data' => serialize($this->getRequest()->getParam('Data')),
                    'date' =>  new Zend_Db_Expr('NOW()')
                );

                $land = new Model_LandingData();
                $land->fill($data);

                try{

                    $land->save();

                    $db = Zend_Registry::get('db');
                    $stmt = $db->prepare("INSERT INTO statistics SET
                            owner_id = :owner_id, landing_id = :lead_id, day = DATE_FORMAT( NOW(), '%Y-%m-%d' ), leads_total = 1, leads_new	= 1
                            ON DUPLICATE KEY UPDATE
                            leads_total = leads_total+1, leads_new	= leads_new+1");

                    $stmt->bindParam('owner_id', $this->getRequest()->getParam('owner_id'));
                    $stmt->bindParam('lead_id', $this->getRequest()->getParam('lead_id'));
                    $stmt->execute();

                    $output = $this->getRequest()->getParam('callback').'(' . Zend_Json::encode(array("ok"),Zend_Json::TYPE_ARRAY) . ');';

                } catch (Exception $e){
                    $output = $this->getRequest()->getParam('callback').'(' . Zend_Json::encode(array("false"),Zend_Json::TYPE_ARRAY) . ');';

                }

                $response = $this->getResponse();
                $response->setBody($output)
                    ->setHeader('content-type', 'text/javascript', true);

            }

        }


    }


    public function registervisitorAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();

        if($this->getRequest()->isGet()){

            if($this->getRequest()->getParam('lead_id') && $this->getRequest()->getParam('owner_id')){

                $db = Zend_Registry::get('db');

                $data  = array(

                    'owner_id' => $this->getRequest()->getParam('owner_id'),
                    'lead_id' => $this->getRequest()->getParam('lead_id'),
                    'ip'      => $_SERVER["REMOTE_ADDR"],
                    'useragent' => $_SERVER['HTTP_USER_AGENT'],
                    'date' =>  new Zend_Db_Expr('NOW()')

                );
                try{

                    $current_table = date('dmY', mktime(0, 0, 0, date("m"), date("d"), date("Y")));
                    $db->insert($current_table.'_statistic', $data);

                    $output = $this->getRequest()->getParam('callback').'(' . Zend_Json::encode(array("ok"),Zend_Json::TYPE_ARRAY) . ');';

                    $response = $this->getResponse();
                    $response->setBody($output)
                        ->setHeader('content-type', 'text/javascript', true);

                    return;

                } catch(Exception $e){


                    $output = $this->getRequest()->getParam('callback').'(' . Zend_Json::encode(array("false"),Zend_Json::TYPE_ARRAY) . ');';

                    $response = $this->getResponse();
                    $response->setBody($output)
                        ->setHeader('content-type', 'text/javascript', true);
                    return;

                }
            }
        }

        $output = $this->getRequest()->getParam('callback').'(' . Zend_Json::encode(array("false"),Zend_Json::TYPE_ARRAY) . ');';

        $response = $this->getResponse();
        $response->setBody($output)
            ->setHeader('content-type', 'text/javascript', true);
        return;

    }


    public function indexAction()
    {

    }


    public function remindAction()
    {
        $form = new Form_RemindPassword();
        $session = new Zend_Session_Namespace('Messages');
        $this->view->title = "Напомнить пароль";
        $this->view->headTitle($this->view->title, 'PREPEND');


        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost() ) ){

                $ownr = new Model_Owner();
                $email = $ownr->getUserByEmail($form->getValue('email'));

                if($email){
                    $ownr = new Model_Owner($email['owner_id']);
                    $ownr->password = rand();
                    $ownr->sendCreateEmail();
                    $ownr->password = sha1($ownr->password);
                    $ownr->save();
                    $session->msg = 'Новый пароль отправлен вам на email';

                } else
                    $session->msg = 'Пользователя с таким адресом не существует';



            }
        }
        //}

        try{
            $this->view->msg = $session->msg;
            $session->msg = '';
        } catch (Exception $e) {
            $session->msg = '';
        }

        $this->view->form = $form;
    }




    public function registerAction(){


        $this->view->title = "Регистрация";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $session = new Zend_Session_Namespace('Messages');
        $form = new Form_addOwner();

        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost() ) ){

                $ownr = new Model_Owner();
                $ownr->fill($form->getValues());

                $ownr->created  = date('Y-m-d H:i:s');
                //$ownr->sendCreateEmail();

                $ownr->password = sha1($ownr->password);

                $ownr->save();

                $session->msg = 'Вы зарегистрированы. Введите логин и пароль для входа';
                $this->_helper->redirector->gotoRoute(array('controller'=>'user', 'action'=>'login'));

            }
        }
        //}

        try{
            $this->view->msg = $session->msg;
            $session->msg = '';
        } catch (Exception $e) {
            $session->msg = '';
        }
        /*
          if(!isset($this->_request->username) && $this->getRequest()->getParam('edit')){
              $form = new Form_Owners();
              $owner = new Model_Owner($this->getRequest()->getParam('edit'));
              $form->populate($owner->populateForm());
          }else
              $form = new Form_addOwner();
          */
        //$this->view->add_owner = $add_owner;

        $this->view->form = $form;

    }

	public function logoutAction(){
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->_helper->redirector('index');	
		
		
	}
    
}