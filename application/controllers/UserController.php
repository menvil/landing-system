<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {

        $this->view->tab = $this->_getParam('action');

        if ($this->_request->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

        }

    }

    public function resetAction()
    {

        $search_form = new Zend_Session_Namespace('search_form');
        unset($search_form->_data);
        $this->_helper->redirector('index');
    }


    public function resetlandingsAction()
    {
        $search_form = new Zend_Session_Namespace('search_formleads');
        unset($search_form->_data);
        $this->_helper->redirector('leads');
    }

    public function resetownersAction()
    {

        $search_form = new Zend_Session_Namespace('search_users');
        unset($search_form->_data);
        $this->_helper->redirector('owners');
    }

    public function loginAction(){
        $session = new Zend_Session_Namespace('Messages');
        $this->view->title = "Вход";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $form = new Form_Login();
        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost())){
                $user = new Model_Owner();
                if($user->authorize($form->getValue('username'), $form->getValue('password'))){
                    if(Zend_Auth::getInstance()->getIdentity()->role == 'operator')
                      $this->_helper->redirector('leads');
                    else
                        $this->_helper->redirector('index');


                } else {
                    $this->view->error = 'Неверные данные авторизации.';
                }
            }
        }

        $this->view->msg = $session->msg;
        $session->msg = '';
        $this->view->form = $form;
    }

    public function indexAction()
    {
        $search_form = new Zend_Session_Namespace('search_form');
        $form_search = new Form_Searches();

        $this->view->title = "Статистика";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $db = Zend_Registry::get('db');
        $select = $db->select()
                        ->from(array('q' => 'statistics'),array('day',
                                                                'visitors_total'=>'SUM(`visitors_total`)',
                                                                'uniques'=>'SUM(`uniques`)',
                                                                'leads_total'=>'SUM(`leads_total`)',
                                                                'leads_new'=>'SUM(`leads_new`)',
                                                                'leads_approved'=>'SUM(`leads_approved`)',
                                                                'leeds_declined'=>'SUM(`leeds_declined`)',
                                                                'income'=>'SUM(`income`)',
                                                                'avrg_cost'=>'FORMAT(income/SUM(`leads_approved`),2)',
                                                                'conversion'=>' FORMAT(SUM(`leads_approved`)/SUM(`uniques`),2)'))

                        ->group('day')
                        ->order('day DESC');


        if($this->getRequest()->isPost()){
            if($form_search->isValid($this->getRequest()->getPost()))
                $search_form->_data = $this->getRequest()->getParams();
            $this->_helper->redirector('index');
        }


        if(isset($search_form->_data) && !empty($search_form->_data) && is_array($search_form->_data)){

            if($search_form->_data['dp1'])
                $select->where('q.day >= ?',date('Y-m-d', strtotime($search_form->_data['dp1'])) );


            if($search_form->_data['dp2'])
                $select->where('q.day <= ?',date('Y-m-d', strtotime($search_form->_data['dp2'])) );


            if($search_form->_data['landing_id'])
                $select->where('q.landing_id = ?', $search_form->_data['landing_id']);

            if(isset($search_form->_data['email']) && Zend_Auth::getInstance()->getIdentity()->role == 'admin'){
                $o = new Model_Owner();
                $u = $o->getUserByEmail($search_form->_data['email']);
                if($u['owner_id'])
                    $select->where('q.owner_id = ?', $u['owner_id']);
            }

            $form_search->populate($search_form->_data);
        }

        if(Zend_Auth::getInstance()->getIdentity()->role != 'admin')
            $select->where('q.owner_id = ?', Zend_Auth::getInstance()->getIdentity()->owner_id);


        //$select->where('q.owner_id = ?',1589);

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($select));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

        $owner = new Model_Owner();
        $owner->getUserBalance(Zend_Auth::getInstance()->getIdentity()->owner_id);
        $this->view->users = $owner->getJsUsers();
        $this->view->form_search = $form_search;

    }

    public function paymentsAction(){

        $this->view->title = "Выплаты";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $payments = new Model_Payments();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($payments->getUserPayments(array(0=>array('owner_id',Zend_Auth::getInstance()->getIdentity()->owner_id,'=')))));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }

    public function newsAction(){

        $this->view->title = "Новости";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $news = new Model_News();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($news->getNews()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }

    public function addnewsAction(){

        $this->view->title = "Добавление новости";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');

        $news = new Model_News();
        $form = new Form_addNews();


        if($this->getRequest()->isPost()){

            if($form->isValid($this->getRequest()->getPost()) &&  !isset($this->_request->id)){

                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');
                $news->save();
                $session->msg = 'Ваши данные изменены';
                $this->_helper->redirector('news');

            } elseif ($form->isValid($this->getRequest()->getPost()) && isset($this->_request->id)){

                $id = $this->getRequest()->getParam('id');
                $news = new Model_News($id);
                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');
                $news->save();
                $session->msg = 'Новость изменена';
                $this->_helper->redirector('news');

            }

        }

        if(isset($this->_request->id)){
            $id = $this->getRequest()->getParam('id');
            $news = new Model_News($id);
            $form->populate($news->populateForm());

        }

        $this->view->form = $form;


    }

    public function deletenewsAction(){
        $session = new Zend_Session_Namespace('Messages');

        if($this->getRequest()->getParam('id')){
            $news = new Model_News($this->getRequest()->getParam('id'));
            $news->delete();
            $session->msg = 'Новость удалена';

            $this->_helper->redirector('news');

        }

    }


    public function addpromoAction(){

        $this->view->title = "Добавление лендинга";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');

        $news = new Model_Landings();
        $form = new Form_addPromo();


        if($this->getRequest()->isPost()){

            if($form->isValid($this->getRequest()->getPost()) &&  !isset($this->_request->id)){

                $news->fill($form->getValues());

                $news->save();
                $session->msg = 'Ваши данные изменены';
                $this->_helper->redirector('promo');

            } elseif ($form->isValid($this->getRequest()->getPost()) && isset($this->_request->id)){

                $id = $this->getRequest()->getParam('id');
                $news = new Model_Landings($id);

                $news->fill($form->getValues());
                $news->save();
                $session->msg = 'Промо изменено';
                $this->_helper->redirector('promo');

            }

        }

        if(isset($this->_request->id)){
            $id = $this->getRequest()->getParam('id');
            $news = new Model_Landings($id);
            $form->populate($news->populateForm());

        }

        $this->view->form = $form;


    }

    public function deletepromoAction(){
        $session = new Zend_Session_Namespace('Messages');

        if($this->getRequest()->getParam('id')){
            $news = new Model_News($this->getRequest()->getParam('id'));
            $news->delete();
            $session->msg = 'Новость удалена';

            $this->_helper->redirector('news');

        }

    }

    public function promoAction(){

        $this->view->title = "Промо";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $news = new Model_Landings();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($news->getPromo()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }



    public function adminpaymentsAction(){
        $paymentssearch_form = new Zend_Session_Namespace('paymentssearch_form');
        $form_searchpayments = new Form_PaymentSearch();

        $this->view->title = "Управление выплатами";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $payments = new Model_Payments();
        $select = $payments->getAllPayments();

        if($this->getRequest()->isPost()){
            if($form_searchpayments->isValid($this->getRequest()->getPost()))
                $paymentssearch_form->_data = $this->getRequest()->getParams();
            $this->_helper->redirector('adminpayments');
        }

        if(isset($paymentssearch_form->_data) && !empty($paymentssearch_form->_data) && is_array($paymentssearch_form->_data)){
            //prn($search_form->_data);
            if($paymentssearch_form->_data['dp1'])
                $select->where('q.date >= ?',date('Y-m-d', strtotime($paymentssearch_form->_data['dp1'])) );


            if($paymentssearch_form->_data['dp2'])
                $select->where('q.date <= ?',date('Y-m-d', strtotime($paymentssearch_form->_data['dp2'])) );


            if($paymentssearch_form->_data['status'])
                $select->where('q.status = ?', $paymentssearch_form->_data['status']);


            $form_searchpayments->populate($paymentssearch_form->_data);
            //print_r(1); exit;
        }

        //echo $select->__toString();




        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($select));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

        $this->view->form_searchpayments = $form_searchpayments;

    }

    public function ownersAction(){

        $this->view->title = "Пользователи";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $search_form = new Zend_Session_Namespace('search_users');
        $form_users = new Form_UsersSearch();

        if($this->getRequest()->isPost()){

            if($form_users->isValid($this->getRequest()->getPost())){
                $search_form->_data = $this->getRequest()->getParams();
            }
            $this->_helper->redirector('owners');
        }

        $owner = new Model_Owner();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($owner->getUsers($search_form->_data)));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

        if(is_array($search_form->_data))
            $form_users->populate($search_form->_data);

        $this->view->users = $owner->getJsUsers();
        $this->view->form_search = $form_users;

    }

    public function profileAction()
    {

        $this->view->title = "Профиль";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');
        $form = new Form_Owners();

        if($this->getRequest()->isPost()){

                if($form->isValid($this->getRequest()->getPost())){

                    $ownr = new Model_Owner(Zend_Auth::getInstance()->getIdentity()->owner_id);

                    if(!$this->_getParam('password'))
                        $ownr->fill($form->getValues(), array('username','password'));

                    else{
                        $ownr->fill($form->getValues(),array('username'));
                        $ownr->password = sha1($ownr->password);
                    }

                    $ownr->modified  = date('Y-m-d H:i:s');

                    $ownr->save();
                    $session->msg = 'Ваши данные изменены';
                    $this->_helper->redirector('profile');
                }


        }

        $owner = new Model_Owner(Zend_Auth::getInstance()->getIdentity()->owner_id);
        $form->populate($owner->populateForm());
        $this->view->msg =  $session->msg;
        $session->msg = "";
        $this->view->form = $form;

    }


    public function leadsAction(){

        $this->view->title = "Статистика лидов";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $search_form = new Zend_Session_Namespace('search_formleads');
        $form_searchleads = new Form_Searchleads();

        if($this->getRequest()->isPost()){
            if($form_searchleads->isValid($this->getRequest()->getPost()))
                $search_form->_data = $this->getRequest()->getParams();
            $this->_helper->redirector('leads');
        }

        $landings = new Model_LandingData();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($landings->getAllLandings($search_form->_data)));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

        if(is_array($search_form->_data))
            $form_searchleads->populate($search_form->_data);

        $this->view->form_search = $form_searchleads;



    }

}

