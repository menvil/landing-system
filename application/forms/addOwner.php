<?php

class Form_addOwner extends Twitter_Form
{

	
	public function __construct()
	{
		
		$this->setName('form_add_owners');
		parent::__construct();

		$username = new Zend_Form_Element_Text('username');
		$username->setLabel('Логин')
			->setRequired(true)
			->addValidator('NotEmpty')
			->addValidator('Alnum')
			->addValidator('Db_NoRecordExists', false, array(
				'table' => 'owners',
				'field' => 'username'
			))
			->addFilter('StringTrim')
			->addFilter('StripTags');


		$password = new Zend_Form_Element_Password('password');
		$password->setLabel('Пароль')
			->setRequired(true)
			->addValidator('NotEmpty');


		$password_confirm = new Zend_Form_Element_Password('password_confirm');
		$password_confirm->setLabel('Введите пароль еще раз')
			->setRequired(true)
			->addValidator('NotEmpty')
			->addPrefixPath('Menvil_Validator', 'Menvil/Validator', 'validate')		
			->addValidator('Passwordconfirm');


		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('Email')
			->addValidator('EmailAddress');


        $owner_name = new Zend_Form_Element_Text('owner_name');
        $owner_name->setLabel('Имя');
        $owner_name->addValidator('NotEmpty')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim');




        $owner_website = new Zend_Form_Element_Text('owner_website');
        $owner_website->setLabel('Website');
        $owner_website->addFilter('StripTags')
            ->addFilter('StringTrim');


        $owner_im = new Zend_Form_Element_Text('owner_im');
        $owner_im->setLabel('IM (ICQ/Skype/Jabber)');
        $owner_im->addFilter('StripTags')
            ->addFilter('StringTrim');


        $owner_payment = new Zend_Form_Element_Text('owner_payment');
        $owner_payment->setLabel('WMZ/WMR');
        $owner_payment->addFilter('StripTags')
            ->addFilter('StringTrim');


        $submit_owner = new Zend_Form_Element_Submit('submit_owner');
        $submit_owner	->setLabel("Регистрация");
       // $submit_owner->setDecorators($this->_buttonDecorators);

        $this->addElements(array($username, $password, $password_confirm, $email, $owner_name,  $owner_website, $owner_im, $owner_payment, $submit_owner));

    }
}