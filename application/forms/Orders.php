<?php

class Form_Orders extends Zend_Form
{
        protected  $_buttonDecorators = array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'),  array('tag' => 'span', 'class' => 'element')),
            array(array('label' => 'HtmlTag'), array('tag' => 'span',  'placement' => 'prepend')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'order_element')),
        );
	
	public function __construct()
	{
		$this->setName('orders_form');
		$this->setAction('/flats/index');
		parent::__construct();
		
		
		$order_select = new Zend_Form_Element_Select('order_select');
		$order_select->addMultiOption('0','Релевантности')->setAttrib('onchange','this.form.submit();');
		$order_select->addMultiOption('price ASC','Цена: по возрастанию');
		$order_select->addMultiOption('price DESC','Цена: по убыванию');
		$order_select->addMultiOption('rooms_count DESC','Кол-во комнат: по убыванию');
		$order_select->addMultiOption('rooms_count ASC','Кол-во комнат: по возрастанию');
								
		$order_select->setDecorators($this->_buttonDecorators);		
		
		$this->addDecorator('htmlTag', array('tag' => 'div', 'class' => 'orderForm'));
		

		$this->addElements(array($order_select));
	}
}
