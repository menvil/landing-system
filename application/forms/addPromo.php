<?php

class Form_addPromo extends Twitter_Form
{


    protected $_textareaRows = 6;
    protected $_textareaCols = 150;


    public function __construct()
    {

        $this->setName('form_add_promo');
        $this->setAttrib("class","form-horizontal");
        parent::__construct();


        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Название')->addValidator('NotEmpty')->setRequired(true)->addFilter('StringTrim');

        $url = new Zend_Form_Element_Text('url');
        $url->setLabel('URL')->addValidator('NotEmpty')->setRequired(true)->addFilter('StringTrim')->addValidator('NotEmpty');

        $description = new Zend_Form_Element_Textarea('description');
        $description->setLabel('Описание')->addValidator('NotEmpty')->setRequired(true)->addFilter('StringTrim')->addValidator('NotEmpty');
        $description->setOptions(array('rows'=>$this->_textareaRows,  'cols'=>$this->textareaCols));

        $costs = new Zend_Form_Element_Text('costs');
        $costs->setLabel('Цена')->addValidator('NotEmpty')->setRequired(true)->addFilter('StringTrim')->addValidator('Between', false,array('min' => 0, 'max' => 999999, 'inclusive' => true));


        $status = new Zend_Form_Element_Select('status');
        $status->setLabel('Статус');
        $status->addMultiOption('1','Активен');
        $status->addMultiOption('0','Скрыт');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit	->setLabel("Сохранить");


        $this->addElements(array($name, $url, $description, $costs, $status, $submit));

    }
}