<?php

class Form_Landings extends Zend_Form
{

	public function __construct()
	{
		
		$this->setName('form_districts');
		parent::__construct();
		
		$_city = Zend_Registry::get('city_code');
		$districts_model = new Model_City($_city['city_id']);
		
		$validator_greater=new Zend_Validate_GreaterThan(0);
		$validator_greater->setMessage('Пожалуйста выберите район', Zend_Validate_GreaterThan::NOT_GREATER);
		
		$district_id = new Zend_Form_Element_Select('district_id');
		$district_id->setLabel('Управление районами:')
					->addValidator($validator_greater,TRUE);
					
		$district_id->setOptions(array('onChange'=>'getDistrict(this)'));
		
		$district_id->addMultiOption('0','Выберите район');
		foreach($districts_model->getDistrictsPairs() as $key=>$opt)
			$district_id->addMultiOption($key,$opt);
		
		$district_name = new Zend_Form_Element_Text('district_name');
		$district_name->addValidator('NotEmpty')
						->setRequired(true)
						->addFilter('StripTags')
						->addFilter('StringTrim');
	
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit	->setLabel("Редактировать");
		
		
		$this->addElements(array($district_id, $district_name, $submit));
		
	}
}