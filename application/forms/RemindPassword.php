<?php

class Form_RemindPassword extends Twitter_Form
{


    public function __construct()
    {

        $this->setName('form_remind');
        $this->setAttrib("class","form-horizontal");
        $this->setAction('/index/remind');
        parent::__construct();


        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email')
            ->addValidator('EmailAddress');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit	->setLabel("Выслать пароль");
        //$submit_owner->setDecorators($this->_buttonDecorators);

        $this->addElements(array($email, $submit));

    }

}