<?php

class Form_addNews extends Twitter_Form
{


    protected $_textareaRows = 6;
    protected $_textareaCols = 150;


	public function __construct()
	{
		
		$this->setName('form_add_news');
        $this->setAttrib("class","form-horizontal");
		parent::__construct();

        $description = new Zend_Form_Element_Textarea('description');

        $description->setLabel('Текст новости')->addFilter('StringTrim')->addValidator('NotEmpty');
        $description->setOptions(array('rows'=>$this->_textareaRows,  'cols'=>$this->textareaCols));


		$submit = new Zend_Form_Element_Submit('submit');
		$submit	->setLabel("Сохранить");
		
		
		$this->addElements(array($description, $submit));
		
	}
}