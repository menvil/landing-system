<?php

class Form_UsersSearch extends Zend_Form
{

    public function __construct()
    {
        $this->setName('userssearch_form');
        $this->setAction('/user/owners');
        parent::__construct();

        $this->setAttrib('class', 'form-inline well well-small');

        $email = new Zend_Form_Element_Text('email');
        $email->setLabel('Email:');
        $email->setAttrib('class','input-medium offset-right');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Показать');
        $submit->setAttrib('class', 'btn btn-primary');

        $reset = new Zend_Form_Element_Reset('reset');
        $reset->setLabel('Сбросить')->setAttrib('onclick',"window.location='resetowners/'");
        $reset->setAttrib('class', 'btn');

        $this->addElements(array( $email, $submit, $reset));
    }
}
