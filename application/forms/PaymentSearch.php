<?php

class Form_PaymentSearch extends Zend_Form
{

    public function __construct()
    {
        $this->setName('paymentssearch_form');
        $this->setAction('/user/adminpayments');
        parent::__construct();

        $this->setAttrib('class', 'form-inline well  well-small');

        $dp1 = new Zend_Form_Element_Text('dp1');
        $dp1->setLabel('c:');
        $dp1->setValue(date( 'd-m-Y', strtotime(Zend_Auth::getInstance()->getIdentity()->created)));
        $dp1->setAttrib('class','input-small offset-right');


        $dp2 = new Zend_Form_Element_Text('dp2');
        $dp2->setLabel('по:');
        $dp2->setValue(date( 'd-m-Y', time()));
        $dp2->setAttrib('class',' input-small offset-right');

        $status = new Zend_Form_Element_Select('status');
        //$status->setIsArray(true);

        foreach(array_merge(array('0'=>'Все'),array('Pending'=>'Pending','Paid'=>'Paid','Declined'=>'Declined')) as $key=>$opt)
            $status->addMultiOption($key,$opt);

        $status->setLabel('Статус: ');


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Показать');
        $submit->setAttrib('class', 'btn btn-primary');

        $reset = new Zend_Form_Element_Reset('reset');
        $reset->setLabel('Сбросить')->setAttrib('onclick','window.location="/user/reset/"');
        $reset->setAttrib('class', 'btn');

        $this->addElements(array( $dp1,  $dp2, $status, $submit, $reset));
    }
}
