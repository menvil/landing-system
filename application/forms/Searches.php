<?php

class Form_Searches extends Zend_Form
{
	public function __construct()
	{
		$this->setName('search_form');
		$this->setAction('/user/index');
		parent::__construct();

        $this->setAttrib('class', 'form-inline well  well-small');

        $landings_model = new Model_Landings();

        $dp1 = new Zend_Form_Element_Text('dp1');
        $dp1->setLabel('c:');
        $dp1->setValue(date( 'd-m-Y', strtotime(Zend_Auth::getInstance()->getIdentity()->created)));
        $dp1->setAttrib('class','input-small offset-right');


        $dp2 = new Zend_Form_Element_Text('dp2');
        $dp2->setLabel('по:');
        $dp2->setValue(date( 'd-m-Y', time()));
        $dp2->setAttrib('class',' input-small offset-right');

		$landing_id = new Zend_Form_Element_Select('landing_id');
        //$landing_id->setIsArray(true);

        //$landing_id->setOptions(array_merge(array('0'=>'Все'),$landings_model->getLandingsPairs()));
        foreach(array_merge(array('0'=>'Все'),$landings_model->getLandingsPairs()) as $key=>$opt)
            $landing_id->addMultiOption($key,$opt);
        $landing_id->setLabel('Лендинг: ');
		//$district_id->setDecorators($this->_elementDecorators_normal);

        if(Zend_Auth::getInstance()->getIdentity()->role == 'admin'){
            $email = new Zend_Form_Element_Text('email');
            $email->setAttrib('class','input-medium');
            $email->setLabel('Email:');

        } else
            $email = null;

        $submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Показать');
        $submit->setAttrib('class', 'btn btn-primary');

		$reset = new Zend_Form_Element_Reset('reset');
		$reset->setLabel('Сбросить')->setAttrib('onclick','window.location="/user/reset/"');
        $reset->setAttrib('class', 'btn');

		$this->addElements(array( $dp1,  $dp2, $landing_id, $email, $submit, $reset));
	}
}
