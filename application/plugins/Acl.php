<?php

class Plugin_Acl extends Zend_Controller_Plugin_Abstract {
	
	private $_controller = array('controller' => 'error', 'action' => 'denied');
	
	public function __construct(){
		$acl = new Zend_Acl();
		//roles
		$acl->addRole(new Zend_Acl_Role('guest'));
        $acl->addRole(new Zend_Acl_Role('cli'));
        $acl->addRole(new Zend_Acl_Role('user'), 'guest');
		$acl->addRole(new Zend_Acl_Role('admin'));
        $acl->addRole(new Zend_Acl_Role('operator'));
		
		//resources
        $acl->add(new Zend_Acl_Resource('ajax'));
        $acl->add(new Zend_Acl_Resource('cron'));
		$acl->add(new Zend_Acl_Resource('index'));
        $acl->add(new Zend_Acl_Resource('user'));
		
		//permissions
		$acl->deny();
		$acl->allow('admin', null);

		//guest right
//		$acl->allow('guest', null);
/*		$acl->deny('guest', 'flats', array(
			'edit','delete','add'
		));
		
		$acl->deny('guest', 'index', array(
			'options','districs','owners','logout'
		));
		
		*/
/*

		$acl->allow('guest', 'flats', array(
			'index','view','reset', 'registervisitor',
		));
*/

        $acl->allow('cli', 'cron', array(
            'stats', 'newtable', 'payments'
        ));

        $acl->allow('guest', 'index', array(
			'index','register', 'registervisitor', 'registerlanding', 'remind'
		));
        $acl->allow('guest', 'user', array(
            'login'
        ));
		
		//user rights
		$acl->allow('user', 'index', array(
			'logout'
		));

        $acl->allow('user', 'user', array(
            'index','profile','news', 'promo', 'reset','payments'
        ));

        //operator rights
        $acl->allow('operator', 'user', array(
            'leads','resetlandings'
        ));

        $acl->allow('operator', 'index', array(
            'logout',
        ));

        $acl->allow('operator', 'ajax', array(
            'showlog','setstatus'
        ));



        /*
                 *
                 *
                $acl->allow('user', 'flats', array(
                    'add','edit'
                ));
        */
        /*$acl->deny('user', 'users', array('login', 'registration'));
          */
		
		Zend_Registry::set('acl', $acl);	
	}
	
	public function preDispatch (Zend_Controller_Request_Abstract $request){
		
		$auth = Zend_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		if ($auth->hasIdentity()){
			$role = $auth->getIdentity()->role;
		} else {
			$role = 'guest';
		}
		
		if(!$acl->hasRole($role)){
			$role = 'guest';
		}


        if (php_sapi_name() == 'cli'){
            $role = 'cli';
        }


        $controller = $request->getControllerName();
        $action = $request->getActionName();

		if(!$acl->has($controller))
			$controller = null;
			
		if(!$acl->isAllowed($role, $controller, $action )){
			$request->setControllerName($this->_controller['controller']);
			$request->setActionName($this->_controller['action']);
		}
	}
}