<?php

class Model_Owner extends Menvil_Model{
	
	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Owners, $id);
	}
	
	public function getOwner(){
		return $this->_row->findParentRow(new Model_DbTable_Cities, 'City'); 
	}
	
	public function populateForm(){
		return $this->_row->toArray();	
	}
	
	public function getAllUsers($status = null){
        if($status == null)
		    return $this->_dbTable->fetchAll();
        else
            return $this->_dbTable->select()->from(array('q'=>$this->_dbTable->getName()))->where('q.activated = ?', $status)->query()->fetchAll();

	}


    public function getUsers($data = null){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('q'=>$this->_dbTable->getName()))
            ->order('q.created DESC');

        if($data == null)
            return $select ;


        if($data['email'])
            $select->where('q.email = ?', $data['email'] );
        else
            $select->where('q.owner_id = ?', 0 );

        return $select;
    }

     public function getUserByEmail($email = null){

        if($email == null)
            return null ;

        //$db = Zend_Registry::get('db');
        $select = $this->_dbTable->select()
            ->from(array('q'=>$this->_dbTable->getName()))->limit(1);

        if($email)
            $select->where('q.email = ?', $email );

        return $select->query()->fetch();
    }

    public function getJsUsers(){

       $users = array();
       $us = $this->_dbTable->fetchAll();
       foreach($us as $u){
           array_push($users, "{owner_id: '$u->owner_id', value: '$u->email'}");
       }

        return '['.implode(',',$users).']';

    }

	public function sendCreateEmail()
	{

		$mail = new Menvil_Mail();
		$mail->addTo($this->_row->email);
		$mail->setSubject('Изменение пароля');
        $mail->setBodyView('remind', array('user' => $this));

        $mail->send();


	}



    public function getUserBalance($user_id = null){

        if($user_id == null)
            return null;

        $db = Zend_Registry::get('db');

        $select = $db->prepare("SELECT (IF((SELECT SUM( s.income ) AS st
                              FROM statistics as s
                              WHERE s.owner_id =$user_id)
                           IS NULL ,
                           0,
                            (SELECT SUM( s.income ) AS st
                            FROM statistics as s
                            WHERE s.owner_id =$user_id))
                            -
                            IF((SELECT SUM(p.amount) AS a
                                FROM payments as p
                                WHERE p.owner_id = $user_id AND p.`status` = 'Paid')
                            IS NULL ,
                            0,
                            (SELECT SUM(p.amount) as a
                              FROM payments as p
                              WHERE p.owner_id = $user_id AND p.`status` = 'Paid'))) AS Balance");

        $select->execute();
        $ret = $select->fetchAll();
        return $ret[0]['Balance'];
    }
	
	public function authorize ($username, $password){
		$auth = Zend_Auth::getInstance();
		$authAdapter = new Zend_Auth_Adapter_DbTable(
			Zend_Db_Table::getDefaultAdapter(),
			'owners',
			'username',
			'password',
			'sha(?) and activated = 1'
		);
		
		$authAdapter->setIdentity($username)
					->setCredential($password);
		$result = $auth->authenticate($authAdapter);
		if($result->isValid()){
			$storage = $auth->getStorage();
			$storage->write($authAdapter->getResultRowObject(null,array('password')));
			return true;
		}
		return false;
	}
	
	
}