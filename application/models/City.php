<?php 

class Model_City extends Menvil_Model{

	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Cities, $id);
	}
	
	public function populateForm(){
		return $this->_row->toArray();	
	}
	
	public function getDistricts(){
		return $this->_row->findDependentRowset(
			new Model_DbTable_Districts,
			'City'
			);
	}
	
	public function getDistrictsPairs(){
		
		$full_districs = $this->getDistricts();
		$array_pairs = array();
		foreach($full_districs as $dstr)
			$array_pairs[$dstr['district_id']] = $dstr['district_name'];
		return $array_pairs;
	}
	
	public function getOwners(){
		return $this->_row->findDependentRowset(
			new Model_DbTable_Owners,
			'City'
			);
	}
	
	public function getOwnersPairs(){
		
		$full_owners = $this->getOwners();
		$array_pairs = array();
		foreach($full_owners as $ownr)
			$array_pairs[$ownr['owner_id']] = $ownr['owner_name'];
		return $array_pairs;
	}
	
	
}