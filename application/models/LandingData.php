<?php

class Model_LandingData extends Menvil_Model{

    public function __construct ($id = null){
        parent::__construct(new Model_DbTable_LandingData, $id);
    }

    public function getOwner(){
        return $this->_row->findParentRow(new Model_DbTable_Owners, 'Owner');
    }

    public function getLandings(){
        return $this->_row->findParentRow(new Model_DbTable_Landings(), 'Landing');
    }

    public function getAllLandings($data = null){
//->setIntegrityCheck(false)
        $select = $this->_dbTable->getAdapter()->select()
            ->from(array('q'=>$this->_dbTable->getName()))
            ->join(array('l' => 'landings'), 'q.lead_id = l.id', array('l.name','l.costs'))
            ->order('q.date DESC');

        if($data == null)
            return $select;


        if($data['dp1'])
            $select->where('q.date >= ?',date('Y-m-d 00:00:00', strtotime($data['dp1'])) );


        if($data['dp2'])
            $select->where('q.date <= ?',date('Y-m-d 24:59:59', strtotime($data['dp2'])) );


        if($data['landing_id'] != '0')
            $select->where('q.lead_id = ?',$data['landing_id']);

        if($data['status'] != '0')
            $select->where('q.status = ?',$data['status']);

        //echo $select->__toString();

        return $select ;
    }

}