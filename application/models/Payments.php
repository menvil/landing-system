<?php

class Model_Payments extends Menvil_Model{

	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Payments, $id);
	}
	
	public function populateForm(){
		return $this->_row->toArray();	
	}
	
	public function getAllPayments(){

        $db = Zend_Registry::get('db');
        $select = $db->select()
            ->from(array('q'=>$this->_dbTable->getName()))
            ->joinLeft(array('o'=>'owners'),'q.owner_id = o.owner_id',array('email','owner_payment'))
            ->order('q.date DESC');
        return $select;
	}

    public function getUserPayments($conditions = array(0=>array(1,1,'=')) )
    {

        $select = $this->_dbTable->select()
            ->from(array('q'=>$this->_dbTable->getName()));

        foreach($conditions as $key=>$cnd){
            $select->where(''.$cnd[0].' '.$cnd[2].' ?', $cnd[1] )->order('q.date DESC');;
        }

        /*
            ->where('owner_id = ?', Zend_Auth::getInstance()->getIdentity()->owner_id);
        */

        return $select ;

    }

	public function getOwner(){
		return $this->_row->findParentRow(new Model_DbTable_Owners, 'Owner'); 
	}
	

	
}