<?php

class Model_News extends Menvil_Model{

    public function __construct ($id = null){
        parent::__construct(new Model_DbTable_News, $id);
    }

    public function populateForm(){
        return $this->_row->toArray();
    }

    public function getAllNews(){
        return $this->_dbTable->fetchAll();
    }


    public function getNews()
    {

        $select = $this->_dbTable->select()
            ->from(array('q'=>$this->_dbTable->getName()))
            ->order('q.date DESC');

        return $select ;

    }

}