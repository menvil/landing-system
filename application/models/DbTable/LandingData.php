<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ivan
 * Date: 07.08.12
 * Time: 18:26
 * To change this template use File | Settings | File Templates.
 */
class Model_DbTable_LandingData extends Zend_Db_Table_Abstract
{

    protected $_name = 'landing_data';

    public function getName(){
        return $this->_name;
    }

    protected $_referenceMap = array(
        'Owner' => array(
            'columns' => 'owner_id',
            'refTableClass' => 'Model_DbTable_Owners',
            'refColumns' => 'owner_id',
            'onDelete' => self::CASCADE
        ),

        'Landing' => array(
            'columns' => 'lead_id',
            'refTableClass' => 'Model_DbTable_Landings',
            'refColumns' => 'id',
            'onDelete' => self::CASCADE
        )
    );

}
