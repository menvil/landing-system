<?php

class Model_DbTable_Payments extends Zend_Db_Table_Abstract {
	
	protected $_name = 'payments';

    public function getName(){
        return $this->_name;
    }

	protected $_referenceMap = array(
		'Owner' => array(
			'columns' => 'owner_id',
			'refTableClass' => 'Model_DbTable_Owners',
			'refColumns' => 'owner_id',
			'onDelete' => self::CASCADE
		)
	);
	
}