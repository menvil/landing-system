<?php

class Model_LandingLog extends Menvil_Model{

    public function __construct ($id = null){
        parent::__construct(new Model_DbTable_LandingLog, $id);
    }

    public function getOwner(){
        return $this->_row->findParentRow(new Model_DbTable_Owners, 'Owner');
    }

    public function getLandings(){
        return $this->_row->findParentRow(new Model_DbTable_LandingData(), 'LandingData');
    }

    public function getLogs($ld_id = null){
        if($ld_id == null)
            return null;

        $select = $this->_dbTable->getAdapter()->select()
            ->from(array('q'=>$this->_dbTable->getName()))
            ->join(array('o' => 'owners'), 'o.owner_id = q.owner_id')
            ->where('q.lead_data_id = ?',$ld_id)
            ->order('q.date ASC');

        return $select->query()->fetchAll();

    }
}

?>