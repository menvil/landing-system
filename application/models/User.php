<?php

class Model_User extends Menvil_Model{

	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Users, $id);
	}
	
	public function populateForm(){
		return $this->_row->toArray();	
	}
	
	public function getAllUsers(){
		return $this->_dbTable->fetchAll();	
	}

    public function getUserBalance($user_id = null){

        if($user_id == null)
            return null;

        $db = Zend_Registry::get('db');

        $select = $db->prepare("SELECT (IF((SELECT SUM( s.income ) AS st
                              FROM statistics as s
                              WHERE s.owner_id =$user_id)
                           IS NULL ,
                           0,
                            (SELECT SUM( s.income ) AS st
                            FROM statistics as s
                            WHERE s.owner_id =$user_id))
                            -
                            IF((SELECT SUM(p.amount) AS a
                                FROM payments as p
                                WHERE p.owner_id = $user_id AND p.`status` = 'Paid')
                            IS NULL ,
                            0,
                            (SELECT SUM(p.amount) as a
                              FROM payments as p
                              WHERE p.owner_id = $user_id AND p.`status` = 'Paid'))) as Balance");




        $d = $select->execute();
        print_r($d);
    }

	public function authorize ($username, $password){
		$auth = Zend_Auth::getInstance();
		$authAdapter = new Zend_Auth_Adapter_DbTable(
			Zend_Db_Table::getDefaultAdapter(),
			'users',
			'username',
			'password',
			'sha(?) and activated = 1'
		);
		
		$authAdapter->setIdentity($username)
					->setCredential($password);
		$result = $auth->authenticate($authAdapter);
		if($result->isValid()){
			$storage = $auth->getStorage();
			$storage->write($authAdapter->getResultRowObject(null,array('password')));
			return true;
		}
		return false;
	}

}