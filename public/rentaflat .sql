-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 13 2012 г., 12:27
-- Версия сервера: 5.1.61
-- Версия PHP: 5.3.6-13ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `rentaflat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `30072012_statistic`
--

DROP TABLE IF EXISTS `30072012_statistic`;
CREATE TABLE IF NOT EXISTS `30072012_statistic` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) unsigned NOT NULL,
  `lead_id` bigint(20) unsigned DEFAULT NULL,
  `ip` varchar(16) NOT NULL,
  `useragent` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `owner_id` (`owner_id`),
  KEY `lead_id` (`lead_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `30072012_statistic`
--

INSERT INTO `30072012_statistic` (`Id`, `owner_id`, `lead_id`, `ip`, `useragent`, `date`) VALUES
(17, 1, 1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.142 Safari/5', '2012-10-13 00:36:17');

-- --------------------------------------------------------

--
-- Структура таблицы `landings`
--

DROP TABLE IF EXISTS `landings`;
CREATE TABLE IF NOT EXISTS `landings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `picture` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `costs` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `landings`
--

INSERT INTO `landings` (`id`, `name`, `url`, `description`, `picture`, `costs`, `status`) VALUES
(1, 'Лид 1', 'http://yandex.ru', 'sldkfaslksd', '', 50, 1),
(2, 'Лид 2', 'http://google.com', 'Test lead', '', 40, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `landing_data`
--

DROP TABLE IF EXISTS `landing_data`;
CREATE TABLE IF NOT EXISTS `landing_data` (
  `ld_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lead_id` bigint(20) unsigned NOT NULL,
  `owner_id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `data` text NOT NULL,
  `details` varchar(255) NOT NULL,
  `income` float NOT NULL,
  `status` enum('New','Processing','Approved','Declined') NOT NULL,
  PRIMARY KEY (`ld_id`),
  KEY `lead_id` (`lead_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `landing_data`
--

INSERT INTO `landing_data` (`ld_id`, `lead_id`, `owner_id`, `date`, `data`, `details`, `income`, `status`) VALUES
(26, 1, 1, '2012-10-13 00:36:24', 'a:2:{s:4:"Name";s:4:"Ivan";s:9:"Last Name";s:5:"Moroz";}', '', 0, 'New');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `date`, `description`) VALUES
(1, '2012-07-15 00:00:00', 'Super news <b>asdfasdf</b>');

-- --------------------------------------------------------

--
-- Структура таблицы `owners`
--

DROP TABLE IF EXISTS `owners`;
CREATE TABLE IF NOT EXISTS `owners` (
  `owner_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `role` enum('user','owner','admin') COLLATE utf8_bin NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `activated` tinyint(4) NOT NULL DEFAULT '1',
  `owner_name` varchar(30) COLLATE utf8_bin NOT NULL,
  `owner_im` varchar(50) COLLATE utf8_bin NOT NULL,
  `owner_rate` tinyint(4) NOT NULL DEFAULT '50',
  `owner_website` varchar(50) COLLATE utf8_bin NOT NULL,
  `owner_payment` varchar(50) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=105 ;

--
-- Дамп данных таблицы `owners`
--

INSERT INTO `owners` (`owner_id`, `username`, `password`, `email`, `role`, `created`, `modified`, `activated`, `owner_name`, `owner_im`, `owner_rate`, `owner_website`, `owner_payment`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'menvil.menvil@gmail.com', 'admin', '2011-03-11 12:16:05', '0000-00-00 00:00:00', 1, '', '', 50, '', ''),
(16, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(17, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(18, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(19, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(20, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(21, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(22, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(23, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(24, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(25, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(26, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(27, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Надежда', '', 50, '', ''),
(28, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(29, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(30, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(31, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(32, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(33, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(34, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(35, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'евгений', '', 50, '', ''),
(36, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(37, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(38, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(39, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(40, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(41, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(42, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(43, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(44, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'б', '', 50, '', ''),
(45, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Без Имени', '', 50, '', ''),
(46, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Дмитрий Калашник', 'Площадь Конституции 1, Дворец Труда, офис 7212', 50, '', ''),
(47, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Алексей', '', 50, '', ''),
(48, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Марина', 'Харьков, Сумская, 25', 50, '', ''),
(49, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Марина', 'Харьков, Сумская, 25', 50, '', ''),
(50, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Марина', 'Харьков, Сумская, 25', 50, '', ''),
(51, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Марина', 'Харьков, Сумская, 25', 50, '', ''),
(52, '', '', '', 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Татьяна Янушкова', '', 50, '', ''),
(53, 'menvil', '987ae76b9828ce6faadd12e5abe265c7b8478ee9', 'menvil@mail.ru', 'user', '2011-03-11 12:21:59', '2012-07-06 20:55:01', 1, 'Мороз Иван', '27342873482', 50, 'asdfasdf', 'sdfsdf'),
(54, 'menvil2', '03b9c5218d6e8570d617e320dccefcd510181365', 'menvil@mail.ru', 'user', '2011-03-11 12:27:07', '0000-00-00 00:00:00', 1, 'Иван Мороз', '', 50, '', ''),
(55, 'menvil3', '03b9c5218d6e8570d617e320dccefcd510181365', 'menvil.menvil@gmail.com', 'user', '2011-03-11 12:28:44', '0000-00-00 00:00:00', 1, 'Иван', '', 50, '', ''),
(58, 'rew', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'nasutochki@gmail.com', 'user', '2011-03-11 13:21:14', '0000-00-00 00:00:00', 1, 'tolik', '', 50, '', ''),
(59, 'tret', '03b9c5218d6e8570d617e320dccefcd510181365', 'menvil.menvil@gmail.com', 'user', '2011-03-11 13:44:53', '0000-00-00 00:00:00', 1, '321', '', 50, '', ''),
(60, 'jobisdone', '091846dab8971bfd85b892b3a4a199d4ea83ea6d', 'jobisdone@i.ua', 'user', '2011-03-12 06:29:54', '0000-00-00 00:00:00', 1, 'Владимир', '', 50, '', ''),
(61, 'teres', '23b19ba806d683e7d58111d0a5a3ee92c8ab85a0', 'teres@mail.ru', 'user', '2011-03-12 06:57:40', '0000-00-00 00:00:00', 1, 'Татьяна', '', 50, '', ''),
(62, 'elenahouse', 'b1f561c68f891bc15f00eec68acd2721c392f948', 'elenahouse@ukr.net', 'user', '2011-03-12 07:24:30', '0000-00-00 00:00:00', 1, 'Юрий, Елена, Сергей', '', 50, '', ''),
(63, 'malanichka', '4f6632191b1c31fc1991f40a78e64800a2daa329', 'malanichka@mail.ru', 'user', '2011-03-12 08:16:01', '0000-00-00 00:00:00', 1, 'Ольга', '', 50, '', ''),
(64, 'trigub', 'd760e16c491bb23d05b567579301b21b394ee5fe', 'trigub@mail.ua', 'user', '2011-03-12 08:27:24', '0000-00-00 00:00:00', 1, 'сергей', '', 50, '', ''),
(65, 'migaled', '9bb965554238c9b8a684b7e2789da05869c7d00a', 'migaled@ukr.net', 'user', '2011-03-12 08:34:23', '0000-00-00 00:00:00', 1, 'Алексей', '', 50, '', ''),
(66, 'danila', 'a8e4b9f3d759361d80a63c9b85576b72759a9d69', 'danila8_77@mail.ru', 'user', '2011-03-12 08:43:24', '0000-00-00 00:00:00', 1, 'Данил', '', 50, '', ''),
(67, 'irinaasad', '54dbdf28ab95a164647ff13d8e4766a97d16ea22', 'irinaasad@meta.ua', 'user', '2011-03-12 10:09:03', '0000-00-00 00:00:00', 1, 'Ирина', '', 50, '', ''),
(68, '11vladislav11', '46510dd95633b11a701e3906a296f5726eb201fa', '11vladislav11@gmail.com', 'user', '2011-03-13 12:36:07', '0000-00-00 00:00:00', 1, 'владислав', '', 50, '', ''),
(69, 'em', 'd1ec1956c5be053328ecbd27c0bb1d287166d73f', 'em_zorg@mail.ru', 'user', '2011-03-14 10:31:15', '0000-00-00 00:00:00', 1, 'Александр', '', 50, '', ''),
(70, 'v80677380188', '8523a0bf4c827fb81ec2c58fc95c9de8c42c9ab5', 'v80677380188@gmail.com', 'user', '2011-03-14 10:39:55', '0000-00-00 00:00:00', 1, 'Лина', '', 50, '', ''),
(71, 'sikorsky', '2fbeb25a909b04a0196d1d2fda6138cd5d46ccb9', 'sikorsky741@gmail.com', 'user', '2011-03-14 12:59:28', '0000-00-00 00:00:00', 1, 'сергей', '', 50, '', ''),
(72, 'karsvet', '0d3bad91daeb794a85130f68a15e68ede7d6b218', 'karsvet@i.ua', 'user', '2011-03-16 09:16:06', '0000-00-00 00:00:00', 1, 'света', '', 50, '', ''),
(73, 'ljubascha', 'a5a3bfba76b059899de9f44d29feb9ec95a8b5dd', 'ljubascha1985@rambler.ru', 'user', '2011-03-16 09:54:49', '0000-00-00 00:00:00', 1, 'Александра', '', 50, '', ''),
(75, 'visenna', '2ddbc28dfc154007c22f1bfb4287b8d787498748', 'visenna.linx@gmail.com', 'user', '2011-03-16 10:22:51', '0000-00-00 00:00:00', 1, 'Анастасия', '', 50, '', ''),
(77, 'lenameles', '9168433dd1702e4019ac6fcc6422746d72797689', 'lenameles@ukr.net', 'user', '2011-03-16 11:50:51', '0000-00-00 00:00:00', 1, 'Лена', '', 50, '', ''),
(78, 'tatiana', '1d43337942634ee5b3c3180a02e127c7fa5ab259', 'tatiana_ya@ukr.net', 'user', '2011-03-16 12:39:05', '0000-00-00 00:00:00', 1, 'Татьяна', '', 50, '', ''),
(79, '27273555', '3bbdeb63425519fa1a7f973bc2de9ff01df84a55', '27273555@mail.ru', 'user', '2011-03-16 12:48:15', '0000-00-00 00:00:00', 1, 'Инна', '', 50, '', ''),
(80, 'ogorev', '9ea2309bd57a643db05ce4604e474fb23d73b06b', 'ogorev@bigmir.net', 'user', '2011-03-16 13:25:52', '0000-00-00 00:00:00', 1, 'Игорь', '', 50, '', ''),
(81, 'iren7mk', '1332dd18ff6fc3908c4ad7bff9fbb3b27ade077a', 'iren7mk@mail.ru', 'user', '2011-03-17 08:08:48', '0000-00-00 00:00:00', 1, 'Ирина', '', 50, '', ''),
(82, 'cbetaua', '6a057bfab811b0b26aea7fff6e81efd0c8a147aa', 'cbetaua@yandex.ua', 'user', '2011-03-17 11:00:45', '0000-00-00 00:00:00', 1, 'Света', '', 50, '', ''),
(83, 'lianatime', '8a6ca3cfc9c5b1b615d9a971e6191386bc6d7806', 'lianatime@mail.ru', 'user', '2011-03-18 10:54:31', '0000-00-00 00:00:00', 1, 'Алёна', '', 50, '', ''),
(84, 'elena', 'e8346d7d7dead906674e550a9c496de018b739ff', 'menvil.menvil@gmail.com', 'user', '2011-03-20 03:44:20', '0000-00-00 00:00:00', 1, 'Елена', '', 50, '', ''),
(85, 'aleksiy888', 'df8e1c74b46a940804b486bbff121e50ea7cb7c7', 'aleksiy888@gmail.com', 'user', '2011-03-20 09:01:00', '0000-00-00 00:00:00', 1, 'Алексей', '', 50, '', ''),
(86, 'ivanov', '74873d05bdf6699cd982775e8151ff88b3306194', 'ivanov-comes@mail.ru', 'user', '2011-03-21 06:59:26', '0000-00-00 00:00:00', 1, 'Денис', '', 50, '', ''),
(87, 'comnata8', 'a4c5379ff6ec21a1d63f2d9a481f74fee5103545', 'comnata8@rambler.ru', 'user', '2011-03-21 10:17:13', '0000-00-00 00:00:00', 1, 'Елена', '', 50, '', ''),
(88, 'puzan', 'ea818c054da91b386fbf72d0e82147a055ac7176', 'puzan.71@mail.ru', 'user', '2011-04-09 06:01:10', '0000-00-00 00:00:00', 1, 'Вика', '', 50, '', ''),
(89, 'olgk', 'afd21ffdd0ecb3d8aff68dae1db48874756b286e', 'olgk08@ukr.net', 'user', '2011-04-09 08:13:07', '0000-00-00 00:00:00', 1, 'Ольга', '', 50, '', ''),
(90, 'lora', '4e94fdfd7a9be1702aedfd01e4663f789c388a01', 'lora-11003@mail.ru', 'user', '2011-04-16 06:44:43', '0000-00-00 00:00:00', 1, 'Лариса', '', 50, '', ''),
(91, 'nade', 'e133f71705b2c7061691f12e615a3b53c9faaac6', 'nade1987@yandex.ru', 'user', '2011-04-20 09:53:37', '0000-00-00 00:00:00', 1, 'Ирина', '', 50, '', ''),
(92, 'shans', '46e5d5b47fa30b4099ba42686c4cfe4d1e1f38f1', 'shans-office@mail.ru', 'user', '2011-04-26 09:35:40', '0000-00-00 00:00:00', 1, 'Сергей', '', 50, '', ''),
(93, 'maks', 'e1223c704a64f4937fc21c52c6ad86df635b1dbe', 'maks127@ukr.net', 'user', '2011-04-29 12:34:52', '0000-00-00 00:00:00', 1, 'Максим', '', 50, '', ''),
(94, 'larissalara', '5f2413aa120bd0d4591ebc53e4c8521bea5bb5b3', 'larissalara9@gmail.com', 'user', '2011-05-08 02:56:16', '0000-00-00 00:00:00', 1, 'Лариса', '', 50, '', ''),
(95, 'proekt', '16641b0f13776e6a06fc1c37559e3a433fa2865d', 'proekt10@gmail.com', 'user', '2011-05-13 08:03:04', '0000-00-00 00:00:00', 1, 'тарас', '', 50, '', ''),
(96, 'dimagold', 'a063377f19ee7f47d7584ba5b54ba16cf963c298', 'dimagold.69@mail.ru', 'user', '2011-05-16 09:06:30', '0000-00-00 00:00:00', 1, 'Дмитрий', '', 50, '', ''),
(97, 'irklienko', '8dc036fb76b39e6056f6c53f81edd819258680c0', 'irklienko@yahoo.com', 'user', '2011-05-16 10:44:10', '0000-00-00 00:00:00', 1, 'Тимофей', '', 50, '', ''),
(98, 'nika', '0b32ec7faac695b569cce1735baf7e8e12825027', 'nika1024@gmail.com', 'user', '2011-05-24 12:54:51', '0000-00-00 00:00:00', 1, 'Вероника', '', 50, '', ''),
(99, 'maf', 'c5191f031ef2f88892aaf87bbc4187d8e713d376', 'maf_vip@ukr.net', 'user', '2011-05-30 10:27:57', '0000-00-00 00:00:00', 1, 'Майя', '', 50, '', ''),
(100, 'andr', '9e85874e2e8bd557b0a7fa576e7e9d8ab33b5708', '0676540201@ukr.net', 'user', '2011-09-15 13:02:28', '0000-00-00 00:00:00', 0, 'Андрей', '', 50, '', ''),
(101, 'arenda200', '04f70c64e2be73c7a4fa572cd0dffaf318d1e808', 'arenda@odessa200.com', 'user', '2011-11-29 08:22:27', '0000-00-00 00:00:00', 1, 'Владимир', '', 50, '', ''),
(102, 'ashram', '436c31dff1473808593f377a8f40c4eefb16f9d3', 'ashram@email.ua', 'user', '2012-07-02 07:24:17', '0000-00-00 00:00:00', 1, 'лена', '', 50, '', ''),
(103, 'apartaments', 'b1dff01b1ca0dcd905e36859ab51066ccf1e1d68', 'apartaments@meta.ua', 'user', '2012-07-02 08:02:18', '0000-00-00 00:00:00', 1, 'Света', '', 50, '', ''),
(104, 'super', '987ae76b9828ce6faadd12e5abe265c7b8478ee9', 'menvil.menvil@gmail.com', 'user', '2012-07-04 20:53:38', '0000-00-00 00:00:00', 1, 'ivan', 'jkhsdf', 50, 'skdf', 'kjshfkjsadf');

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `amount` int(11) NOT NULL,
  `details` varchar(100) COLLATE utf8_bin NOT NULL,
  `status` enum('Pending','Paid','Declined','Credited') COLLATE utf8_bin NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`id`, `owner_id`, `date`, `amount`, `details`, `status`) VALUES
(1, 53, '2012-07-12 17:23:28', 100, 'super payment', 'Pending'),
(2, 53, '2012-07-14 11:00:00', 200, 'test againe', 'Pending');

-- --------------------------------------------------------

--
-- Структура таблицы `statistics`
--

DROP TABLE IF EXISTS `statistics`;
CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) unsigned NOT NULL,
  `landing_id` bigint(20) unsigned NOT NULL,
  `day` date NOT NULL,
  `visitors_total` int(11) NOT NULL,
  `uniques` int(11) NOT NULL,
  `leads_total` smallint(6) NOT NULL,
  `leads_new` smallint(6) NOT NULL,
  `leads_approved` smallint(6) NOT NULL,
  `leeds_declined` smallint(6) NOT NULL,
  `income` int(11) NOT NULL,
  `status` enum('earned','pending','paid','declined') NOT NULL DEFAULT 'earned',
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id_2` (`owner_id`,`landing_id`,`day`),
  KEY `owner_id` (`owner_id`),
  KEY `landing_id` (`landing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `statistics`
--

INSERT INTO `statistics` (`id`, `owner_id`, `landing_id`, `day`, `visitors_total`, `uniques`, `leads_total`, `leads_new`, `leads_approved`, `leeds_declined`, `income`, `status`) VALUES
(14, 1, 1, '2012-10-13', 0, 0, 1, 1, 0, 0, 0, 'earned');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(40) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `role` enum('user','admin') CHARACTER SET latin1 NOT NULL DEFAULT 'user',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  `code` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `created`, `modified`, `activated`, `code`) VALUES
(11, 'admin', '00154321d63701d0360585ab5d340d56c6783785', 'admin@nasutochki.com', 'admin', '2010-07-17 12:45:20', NULL, 1, '4c417bb0f2447');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `30072012_statistic`
--
ALTER TABLE `30072012_statistic`
  ADD CONSTRAINT `30072012_statistic_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`owner_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `30072012_statistic_ibfk_3` FOREIGN KEY (`lead_id`) REFERENCES `landings` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `landing_data`
--
ALTER TABLE `landing_data`
  ADD CONSTRAINT `landing_data_ibfk_1` FOREIGN KEY (`lead_id`) REFERENCES `landings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `landing_data_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`owner_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`owner_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `statistics`
--
ALTER TABLE `statistics`
  ADD CONSTRAINT `statistics_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`owner_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
